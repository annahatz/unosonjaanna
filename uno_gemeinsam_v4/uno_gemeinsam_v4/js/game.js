$(document).ready(function () {

    /*
    TODO:
       
    - Meldung dass man bei den Farbwechslern keine Farbe auswählen darf, die man noch hat - Anna
    
    */

    // merken uns die spiel id
    let game

    let players = {} // Mapping zwischen SpielerInnen Name und ihrem div
    let playerOrder // SpielerInnenamen in ursprünglicher Reihenfolge
    let currentPlayer // speichert aktive/n Spieler/in

    // true: aktion kann ausgeführt werdne
    // false: kann keine aktion ausführen da noch andere aktiv ist
    let playable = false


    // oft verwendete div referenz
    let $topCard = $('#topCard')
    let $draw = $('#draw')
    let $colorPicker = $('#colorPicker')
    let $reloadPage = $('#reloadPage')
    isTopCard = new Boolean("false")

    let direction = 1 // fuer spielerInnenwechsel entweder +1/-1

    // karten die farbwaehler benoetigen
    let colorPickerCards = [13, 14]
    // skip player + karten
    let skipCards = [10, 13]
    // umgekehrte spielerInnen reihenfolge
    let reverseCards = [12]

    // Mapping zwischen Kartenfarben und Bootstraphintergrundfarben
    // (eigentlich Bootstrap Hintergrundfarbklassen)
    let colorMap =
    {
        Black: 'bg-secondary',
        Red: 'bg-danger',
        Blue: 'bg-primary',
        Green: 'bg-success',
        Yellow: 'bg-warning'
    }

    // Basis URL für API Aufrufe
    let baseURL = 'http://nowaunoweb.azurewebsites.net'

    // Modalen Dialog öffnen um Namen einzugeben
    $('#playerNames').modal()

    // Funktion zur Überprüfung, dass alle 4 Namen unterschieldlich und nicht leer sind
    // true zurückliefern wenn wir 4 unterschiedliche namen
    // false falls nicht
    let validatePlayerNames = function (form) {
        let fields = $("input:text", form)
        // die values in einem array haben
        // konvertiert mein feld mit den input textfeldern
        // in ein neues feld mit den werten
        let values = fields.map(function () {
            return this.value
        }).filter(function () {
            //Herausfiltern der leeren Elemente (= Selektierne die nicht leeren elemente)
            return this.length > 0
        }).get()

        return (new Set(values)).size == fields.length
    }

    // nach jeder tasteneingabe im formular überprüfen ob
    // 4 eindeutige spielerInnennamen vorhanden sind
    $('#playerNamesForm').on('keyup', function (evt) {
        //console.log(evt)
        $('#playerNamesSubmit').prop('disabled',
            !validatePlayerNames(evt.currentTarget))
        // alternative variante
        //$('button', evt.currentTarget)...
    })

    $('#playerNamesForm').on('submit', function (evt) {
        // Formular absenden verhindern
        //console.log(evt)
        evt.preventDefault()
        let names = $('input:text', evt.currentTarget)
            .prop('disabled', true).map(function () {
                return this.value
            }).get()
        //console.log(names)

        let request = $.ajax({
            url: baseURL + '/api/game/start',
            method: 'POST',
            data: JSON.stringify(names),
            contentType: 'application/json',
            dataType: 'json'
        })

        // Spiel starten erfolgreich
        request.done(function (data) {
            console.log(data)
            // Blenden SpielerInnen Namen Dialog aus
            $('#playerNames').modal('hide')
            // Init das Spiel
            initGame(data)
        })

        // Spiel starten fehlgeschlagen
        request.fail(function (msg) {
            console.log("Error in request ", msg)

            //Eingabefelder wieder aktivieren
            let names = $('input:text', evt.currentTarget)
                .prop('disabled', false).map(function () {
                    return this.value
                }).get()
        })

    }
    )

    $reloadPage.on('click', function () {
        location.reload()
    })
    // Spielfeld initialisieren
    let initGame = function (data) {
        // speichere globale spiel ide
        game = data.Id

        let $board = $('#board')

        isTopCard = true
        $topCard.append(createCard(data.TopCard))
        isTopCard = false

        // click event handler für Abhebebutton
        $draw.on('click', function (event) {
            playable = false

            // abhebe button inaktiv schalten bis request abgeschlossen
            $draw.prop('disabled', true)
            //draw card request schicken
            let request = $.ajax({
                url: baseURL + '/api/Game/DrawCard/' + game,
                method: 'PUT'
            })
            // request fehlgeschlagen nichts machen
            request.fail(function (msg) {
                playable = true

                //abhebe button wieder aktivieren
                $draw.prop('disabled', false)
                console.log('Error drawing card', msg)
            })
            // request erfolgreich

            //dazu Value aus data holen und addieren
            request.done(function (data) {
                console.log('Draw card', data)

                //erzeuge spielbare karte
                let $card = createPlayableCard(data.Card, data.Player)
                // haenge karte zum spieler
                $('.deck', players[data.Player]).append($card)


                // Punkte der abgehobenen Karte hinzufügen
                let $points = $('.points', players[currentPlayer])
                let score = parseInt($points.text()) + parseInt(data.Card.Score)//parseInt weil $points.text ist STring!
                $points.text(score)

                // zum naechsten spielerIn wechseln
                updateCurrentPlayer(data.NextPlayer)
                playable = true
                $draw.prop('disabled', false) // abhebe button wieder aktivieren

            })
        })


        // alle spielerInnen initialisieren
        data.Players.forEach(function (player) {

            let $player = createPlayer(player)

            //mappen spielerInnen name zu ihrem div
            players[player.Player] = $player

            $('#playerList').before($player)
        })
        console.log(players)
        // array mit spielernamen
        playerOrder = data.Players.map(function (pl) {
            return pl.Player
        })

        // auf aktive spielerIn setzen
        updateCurrentPlayer(data.NextPlayer)

        $board.removeClass('invisible')

        playable = true

    }


    let updateCurrentPlayer = function (pl, score) {
        // pruefen ob wir nicht auf schon aktuellen spieler wechslen
        if (pl === currentPlayer) {
            console.log('No update - this player already active')
            return
        }
        currentPlayer = pl
        //namen oben in nav bar setzen
        $('#currentPlayer').text('es spielt:  ' + currentPlayer)

        // aktiven spielerIn erhält rahmen, beim rest entfernen
        Object.keys(players).forEach(function (name) {
            if (name === currentPlayer) {
                // bei aktiver spielerIn rahmen hinzufuegen
                players[name].removeClass('deck-inactive').addClass('deck-active')//.addClass('border-primary')
            }
            else {
                // bei allen anderen rahmen entfernen
                players[name].removeClass('deck-active').addClass('deck-inactive') //('border').removeClass('border-primary')
            }
        })

        // punkte aktualisieren, wenn keine spezifiziert sind wird nichts verändert
        $('.points', players[pl]).text(score)

    }

    // Erzeugen eine/n Spieler/in auf Basis der Server Antwort
    let createPlayer = function (player) {
        // Template auswählen, Text davon auslesen und jQuery Node
        // erzeugen
        let $player = $($('#tmplPlayer').text())
        // Name setzen
        $('.card-title', $player).text(player.Player)
        // Punkte anzeigen
        $('.points', $player).text(player.Score)

        let $deck = $('.deck', $player)
        // jede karte erzeugen und ins Deck hängen
        player.Cards.forEach(function (card) {
            let $card = createPlayableCard(card, player.Player)
            $deck.append($card)
            fadeInCard($card)
        })

        return $player
    }

    let shakeCard = function ($card) {
        $card.addClass('shake')
        //Klasse entfernen damit wiederholt funktioniert
        setTimeout(function () {
            $card.removeClass('shake')
        }, 1000)
    }

    let fadeInCard = function ($card) {
        $card.addClass('fadeIn')
        setTimeout(function () {
            $card.removeClass('fadeIn')
        }, 2000)
    }

    let createPlayableCard = function (card, player) {
        let $card = createCard(card)

        // click event listener fuer karte
        $card.on('click', function (event) {

            // Ueberpruefen ob Karte der aktiven SpielerIn
            if (player !== currentPlayer) {
                shakeCard($card)
                return
            }
            if (!playable) // falls noch eine aktion im gange ist
            {
                return
            }

            $card.attr('id', 'activeCard')

            var posTopCard = $('#topCardImg').offset()
            x = posTopCard.left /* + 37 */
            console.log("x: " + x)

            var posTopCardPos = $('#topCardImg').position()
            y = posTopCardPos.top /* - 7 */
            console.log("y: " + y)

            var posActiveCard = $('#activeCard').offset()
            xA = posActiveCard.left

            var posActiveCardY= $('#activeCard').offset()
            yA=posActiveCard.top

            console.log("xA: " + xA)

            $('#activeCard').animate({
                left: x - xA,
                top: y-yA+155,
            }, 1000)

            playable = false
            if (colorPickerCards.includes(card.Value)) {
                // muessen farbidalog anzeigen
                // fuer farbwahl buttons auf klick reagieren
                $('button', $colorPicker).on('click', function (event) {
                    // event handler entfernen
                    $('button', $colorPicker).off('click')
                    // dialog ausblenden
                    $colorPicker.modal('hide')
                    console.log(event)


                    sendCard(card, $card, $(event.currentTarget).data('color'))

                })
                $colorPicker.modal('show') // farbdialog anzuzeigen

            }
            else {
                // muessen keinen farbidalog anzeigen
                sendCard(card, $card)
            }
        })

        return $card
    }

    // Erzeuge ein Kartenobjekt auf Basis der Vorlage
    // liefert diese Karte als Rückgabwert
    let createCard = function (card) {
        // Template auswählen, Text davon auslesen und 
        // jquery node erzeugen
        let $card = $($('#tmplCard').text())
        // Hintergrundfarbe setzen, Titel und Untertitel setzen
        /*  $card.addClass(colorMap[card.Color])
         $('.card-title', $card).text(card.Value)
         $('.card-subtitle', $card).text(card.Text) */
        let cardUrl = "img/cards/" + card.Color + card.Value + ".png";

        if (isTopCard == true) {
            $card.attr('id', 'topCardImg')
        }

        $card.attr('src', cardUrl)
        return $card
    }

    // sende zu spielend karte an server
    let sendCard = function (card, $card, wildColor) {
        //abhebe button inaktiv schalten

        $draw.prop('disabled', true)
        console.log('Playing card', card, $card)
        // url aubauen fuer request
        let url = baseURL + '/api/Game/PlayCard/' + game + '?value=' +
            card.Value + '&color=' + card.Color + '&wildColor='
        // falls auf wildcolor was oben steht dann anhängen
        if (wildColor) {
            url = url + wildColor
        }
        let request = $.ajax({
            url: url,
            method: 'PUT',
            dataType: 'json'
        })

        // request erfolgreich
        request.done(function (data) {

            // schauen ob error in response
            if (data.hasOwnProperty('error')) {
                console.log('Error at playing card', data.error)
                shakeCard($card)
                $draw.prop('disabled', false)
                playable = true
                return
            }
            // wenn wir hier sind hatten wir anscheinend keinen error :)

            // falls ein farbwechsel dann abgelegte kart einfärben
            if (wildColor) {
                card.Color = wildColor
            }

            // am ablagestapel alte karte entfernen und durch neue karte
            // ersetzen

            setTimeout(function () {
                isTopCard = true
                $topCard.empty().append(createCard(card))
                isTopCard = false
                $card.remove() // karte aus der hand entfernen

                //spielrichtungsaenderung beruecksichtigen
                if (reverseCards.includes(card.Value)) {
                    direction = -direction
                }

                // spieler die übersprungen werden und neue karten bekommen
                if (skipCards.includes(card.Value)) {
                    console.log("start")
                    updatePlayerCards(getSkippedPlayer())

                    let indexOfSkippedPlayer = playerOrder.indexOf(currentPlayer) + direction
                    let skippedPlayer = playerOrder[indexOfSkippedPlayer]

                }
                console.log("Played card", data)
                // hat person gewonnen?

                // falls spielerin letzte karte gespielt hat -> siegerin

                if ($('.deck', players[currentPlayer]).children().length == 0) {
                    //players[currentPlayer].addClass('bg-success')
                    players[currentPlayer].removeClass('deck-active').addClass('blink-bg')
                    $('#gameWon').modal('show')
                }

                // score div der spielerin die gerade karte gespielt hat
                let $points = $('.points', players[currentPlayer])
                let score = $points.text() - card.Score
                console.log(card.Score)
                $points.text(score)
                // naechste spielerin weiterschalten
                updateCurrentPlayer(data.Player, data.Score)

                $draw.prop('disabled', false)
                playable = true

            }, 1000)
        })


        // request schlug fehl
        request.fail(function (msg) {
            console.log('Server denied card', card, msg)
            shakeCard($card)

            $draw.prop('disabled', false)
            playable = true
        })

    }

    // liefere uns namen der uebersprungen person
    let getSkippedPlayer = function () {
        // liefert index von aktueller spielerIn
        let currentIndex = playerOrder.indexOf(currentPlayer)
        let newIndex = (currentIndex + direction) % playerOrder.length
        if (newIndex < 0) {
            newIndex = newIndex + playerOrder.length
        }


        return playerOrder[newIndex]
    }
    // alle karten einer spielerIn abfragen
    let updatePlayerCards = function (player) {
        let url = baseURL + '/api/game/getCards/' + game + '?playerName=' + player
        console.log(url)
        let request = $.ajax(
            {
                url: url,
                method: 'GET',
                dataType: 'json'
            }
        )
        request.done(function (data) {
            // html bereich auswählen in dem karten sind
            let $deck = $('.deck', players[player])
            // spielkarten entfernen
            $deck.empty()
            // alle spielkarten erzeugen
            data.Cards.forEach(function (card) {
                let $card = createPlayableCard(card, player)
                $deck.append($card)
                shakeCard($card)
            })
            $('.points', players[player]).text(data.Score);
        })
        request.fail(function (msg) {
            console.error(msg)
        })

    }

})